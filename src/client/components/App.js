import React, {Component} from 'react';
// import '../components-styles/App.css';
import styled, {keyframes} from 'styled-components';

const Title = styled.h1`
  font-size:24px;
  text-align:center;

`;

const fadeIn = keyframes`
0% {
  background-position: 0% 50%
}
50% {
  background-position: 100% 50%
}
100% {
  background-position: 0% 50%
}
`;



const AppContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  width: 100%;
  height: 100%;
  font-size: 40px; 
  background: linear-gradient(-45deg,#ff3d00,#FF9800,#03A9F4,#9C27B0);
  background-size: 400% 400%; 
  animation: ${fadeIn} 5s linear infinite; 
`

class App extends Component {
  render(){
    return ( 
      <AppContainer>
        <Title>  Hello, World ! </Title>
      </AppContainer>
     )
  }
}

export default App;