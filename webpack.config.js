const path = require("path");
const webpack = require("webpack");
const bundlePath = path.resolve(__dirname, "dist/");
const nodeExternals = require('webpack-node-externals');

module.exports = {
  entry: "./src/server/index.js",
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename:'server.js',
    publicPath: '/'
  },
  target:'node',
  externals: nodeExternals(),
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: `'production'`
      }
    })
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: { presets: ['env'] }
      } 
    ]
  }
};